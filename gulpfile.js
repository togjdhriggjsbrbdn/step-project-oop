const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const rename = require("gulp-rename");
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const nunjucks = require('gulp-nunjucks-render');

function cleandev() {
    return gulp.src('./dist','./*.html', {read: false})
        .pipe(clean())
}

function buildhtml () {
    return gulp.src('./src/*.html')
        .pipe(nunjucks({
            path: 'src/'
        }))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream());
}

function js () {
    return gulp.src('./src/js/*.js')
        .pipe(gulp.dest('./dist/js'))
}

function img () {
    return gulp.src('./src/img/*.png')
        .pipe(gulp.dest('./dist/img'))
}

function scripts () {
    return gulp.src(['src/sections/2_modal/**/*.js','src/sections/1_main/**/*.js'])
        .pipe(concat('all.js'))
        .pipe(rename(function (path) {
            path.extname = ".min.js";
        }))
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream());
}

function forSass() {
    return gulp.src('./src/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cleanCSS({level: 2}))
        .pipe(autoprefixer({
            browsers: ['> 0.1%'],
            cascade: false
        }))
        .pipe(rename(function (path) {
            path.extname = ".min.css";
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch('./src/**/*.scss', forSass);
    gulp.watch('./src/**/*.js', scripts);
    gulp.watch('./src/**/*.html', buildhtml);
}

gulp.task('cleandev', cleandev);
gulp.task('buildHtml', buildhtml);
gulp.task('scripts', scripts);
gulp.task('sass', forSass);
gulp.task('watch', watch);
gulp.task('jq', js);
gulp.task('img', img);
gulp.task('build', gulp.series('cleandev', gulp.series(img, buildhtml, js, scripts, forSass)));
gulp.task('dev', gulp.series('build', watch));
