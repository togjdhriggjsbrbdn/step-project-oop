function makeDragonDrop(cardTarget) {
    let card = cardTarget;
    function move(e) {
        let cord = card.getBoundingClientRect();
        let dek = desk.getBoundingClientRect();
        if ((cord.x - 20 - dek.x) < 0) {
            card.mousePositionX = e.clientX + card.offsetLeft - 20;
        }
        if ((cord.y - 20 - dek.y) < 0) {
            card.mousePositionY = e.clientY + card.offsetTop - 20;
        }
        if (((dek.x + dek.width) - (cord.x + cord.width + 20)) < 0) {
            card.mousePositionX = (card.offsetLeft + cord.width - dek.width) + e.clientX + 30;
        }
        if (((dek.y + dek.height) - (cord.y + cord.height + 20)) < 0) {
            card.mousePositionY = (card.offsetTop + cord.height - dek.height) + e.clientY + 30;
        }
        card.style.transform = `translate(${e.clientX - card.mousePositionX}px,${e.clientY - card.mousePositionY}px)`;
    }

    card.addEventListener('mousedown',(e)=>{
        if (card.style.transform) {
            const transforms = card.style.transform;
            const transformX = parseFloat(transforms.split('(')[1].split(',')[0]);
            const transformY = parseFloat(transforms.split('(')[1].split(',')[1]);
            card.mousePositionX = e.clientX - transformX;
            card.mousePositionY = e.clientY - transformY;
        } else {
            card.mousePositionX = e.clientX;
            card.mousePositionY = e.clientY;
        }
        document.addEventListener('mousemove',move);
    });

    document.addEventListener('mouseup', e => {
        document.removeEventListener('mousemove',move);
    });
}
function findCard() {
    if(document.querySelectorAll('.card').length){
        document.querySelectorAll('.card').forEach((item)=>{
            makeDragonDrop(item);
        })
    }
}

class Cardiologist extends Visit {
    constructor(name, date, purpose, comment, age, pressure, index, diseases) {
        super("Cardiologist", name, date, purpose, comment, '#66d9e5');
        this.age = {
            description: 'Age:',
            text: age
        };
        this.pressure = {
            description: 'Pressure:',
            text: age
        };
        this.index = {
            description: 'BMI:',
            text: age
        };
        this.diseases = {
            description: 'Diseases:',
            text: age
        };
        this.additInfoArr.push(this.age);
        this.additInfoArr.push(this.pressure);
        this.additInfoArr.push(this.index);
        this.additInfoArr.push(this.diseases);
    }
}
class Dentist extends Visit {
    constructor(name, date, purpose, comment, prevDate) {
        super("Dentist", name, date, purpose, comment, '#7be57b');
        this.prevDate = {
            description: 'Recent visit date:',
            text: prevDate
        };
        this.additInfoArr.push(this.prevDate);
    }
}
